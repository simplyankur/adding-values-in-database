from db_test import db


class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50), unique=False, nullable=False)
	email = db.Column(db.String(50), unique=False, nullable=False)
	dob = db.Column(db.DateTime)
	gender = db.Column(db.String(50))

	def __repr__(self):
		return '<User %r>' % self.name