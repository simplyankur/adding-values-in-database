from flask import Flask, render_template, url_for, request
from datetime import datetime

#Importing sqlalchemy
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///test.db"
db = SQLAlchemy(app)




class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50), nullable=False)
	email = db.Column(db.String(50), unique=False, nullable=False)
	dob = db.Column(db.DateTime)
	gender = db.Column(db.String(50))

	def __repr__(self):
		return '<User %r>' % self.name

class Post(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(50), unique=True, nullable=False)
	body = db.Column(db.Text, unique=True, nullable=False)
	user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
	user = db.relationship('User', backref=db.backref('posts', lazy=True))

	def __repr__(self):
		return '<Post %r>' % self.title


@app.route("/", methods=['GET','POST'])
def home():
	if request.method == 'POST':
		name = request.form.get('name')
		email = request.form.get('email')
		dob = datetime.strptime(request.form.get('dob'), "%Y-%m-%d")
		gender = request.form.get('gender')

		new_user = User(name=name,email=email,dob=dob,gender=gender)
		db.session.add(new_user)
		db.session.commit()

	return render_template('index.html', title='HOME')


@app.route("/posts", methods=['POST','GET'])
def posts():
	all_user=[]
	if request.method == 'POST':
		all_user = User.query.all()
		title = request.form.get('title')
		body = request.form.get('body')
		user_selection = request.form.get('user_selection')

		new_post = Post(title=title, body=body, user_id=user_selection)
		db.session.add(new_post)
		db.session.commit()
		
		
	return render_template('posts.html', title='POSTS', all_user=all_user)



if __name__ == "__main__":
	app.run(debug=True)